#!/bin/bash
display () {
	clear;	#this function clears the screen then shows a basic display of all files and folders 
	ls | cat;
}

displayd() {
	clear; #this function clears the screen and displays detail of all files and folders
	ls -l | awk '
		{
			for (i=5; i<=NF; i++)
			{
				printf $i "\t"
			}
						
			printf "\n"
		}';	
}

sortsize() {
	#sortbysize
	clear;	#this function clears the screen and displays detail of all files and folders but also sorts the size of the what is displayed		
	ls -lSh | awk '
	{
		for (i=5; i<=NF; i++)
		{
			printf $i "\t"
		}
						
		printf "\n"
	}';
}

sortbydate() {
	#sortbydate
	clear; #this function clears the screen and displays detail of all files and folders but also sorts the date of the what is displayed
	ls -lth | awk '
	{
		for (i=5; i<=NF; i++)
		{
			printf $i "\t"
		}
						
		printf "\n"
	}';
}

finds(){
	#command to find file
	find $tests -name *$s*;
}

delete(){
	#command to delete file
	echo $1 "Has been deleted";
	rm $1 ;
}

showtree(){
	#this function shows the current paths tree structure
	clear;
	ls -R | grep ":$" | sed -e 's/:$//' -e 's/[^0][^\/]*\//--/g' -e 's/^/   /' -e 's/-/|/';
}

changes(){
	#this function changes the path to a new one or errors and displays a custom error
	cd $1  &> /dev/null || echo "Directory does not exist or permission denied";
}

filedisplay(){
	#this function finds a file or errors and displays a custom error
	cat $1 2>/dev/null || echo "Directory does not exist or permission denied";
}

helps(){
	#this is a help command if someone gets stuck with the commands
	clear;
        echo "Commands are";
        echo "display: will show all files and folders in directory";
        echo "display detail: will show all files and folders in directory with detail";
        echo "display detail (ssize OR sdate): with sort the detailed list buy date or size of file";
        echo "find: with search directory for files containing what was entered";
        echo "showt: will display the directory tree from your current directory";
	echo "delete: will delete the specified file followed after the delete command from your current directory";
        echo "change up: will take you one directory up from where you are";
        echo "change spec : will change to a specific directory follow this command with the directory you wish to travel to";
        echo "fdisplay: will display the contents of a specified file follow this command with the file you wish to display";
        echo "q: will quit the directory";
}

#start of the program clears the terminal window
clear;
while : #start of the while loop this continues indefinatly until a break function is called in the loop
do
    tests=$(pwd) #storage of the current command line 
    pwd #dispay to the user thier current path 
    read -p 'Enter Command: ' f s t; #the input from the user this can be 3 arguments used for refined commands
    if [ $f == "display" ]; then #read the first argument if its display begin display command
        clear; #clear the screen for display of infomation
	if [ -z "$s" ]; then #if no other arguments are passed afterwards then do the basic display
        	#display commanad here
		display #display function called
        else	#with the more arguments the while loop detirmes what it is required to do
		if [ $s == "detail" ]; then #if the second argument is detail begin drilling this down further to work out what to do
			if [ -z "$t" ]; then # if only detail is present then detail just needs to be shown
        			#display detail here
				displayd #display detail command shown
			else
				if [ $t == "ssize" ]; then #begin sorting by size
					sortsize
				elif [ $t == "sdate" ];then # begin sorting by date
					sortbydate
				else
					echo "The command:" $t "does not exist."; # if the third passed argument is not one of the ones above show error message
				fi
			fi
        	else
			echo "The command:" $s "does not exist."; #if a second argument is passed and it isnt matched with any display error	
        	fi
        fi
    elif [ $f == "find" ]; then #calls the find function
        clear;
	if [ -z "$s" ]; then #if a file is not specified a error with display to the user informing them that no file was displayed
		echo "Error file not specified";
	else
		finds #call the find function 
	fi
    elif [ $f == "delete" ]; then # calls the delete function
	clear;
	if [ -z "$s" ]; then	
		echo "Error file not specified"; #if the second argument has nothing in it show error
	else
		delete $s #calls delete function passing through the argument that should be the path name of the file to be deleted
	fi
    elif [ $f == "showt" ]; then 
	showtree #function to show the tree structure of the path
    elif [ $f == "change" ]; then #change directory function this has 2 portions on to simply move up or to change to a specific place
        clear;
	if [ -z "$s" ]; then	
		echo "No specification of where change is occuring"; # if nothing shown in the second argunment show error
	else
		if [ $s == "up" ]; then
			cd .. #if the user has specified up will take the user up one path
		elif [ $s == "spec" ]; then # if the second argunment in spec then begin the specific path
			if [ -z "$t" ]; then	
				echo "No specification of where to go has been put"; # if nothing is in the third argument then display error
			else
				changes $t #function that changes the directory based on the passed variable 
			fi
		else 
			echo "The command:" $s "does not exist."; # if the second argument is not a command then display error
		fi
	fi
    elif [ $f == "fdisplay" ]; then #function that display contents of the file
        clear;
	if [ -z "$s" ]; then	
		echo "Error file not specified"; #if no file is specifed show error screen
	else
		filedisplay $s #call the file display function with the passed variable 
	fi
    elif [ $f == "help" ]; then
	helps # calls the help function 
    elif [ $f == "q" ]; then #this command will break the while loop exiting the directory explorer
	clear;        
	break;
    else
        echo "The command:" $f "does not exist."; #if the user has entered the first argument and does not match anything then display a error
    fi
done
   
